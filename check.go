package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"os"
)

var db = dataBase{"node.db"}
func GetVerify() []Verify  {
	return db.extractAllVerify()
}


func InsertFileHash(file_name string) string {

	checksum := FileMD5(file_name)



	return checksum
}

// MD5 - Превращает содержимое из переменной data в md5-хеш
func MD5(data string) string {
	h := md5.Sum([]byte(data))
	return fmt.Sprintf("%x", h)
}

// FileMD5 создает md5-хеш из содержимого нашего файла.
func FileMD5(path string) string {
	h := md5.New()
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	_, err = io.Copy(h, f)
	if err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", h.Sum(nil))
}