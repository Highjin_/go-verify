package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"io/ioutil"
	"os"
)

type dataBase struct {
	dbName string
}

var DB = dataBase{"node.db"}

func CreateTables(dbname string) {
	db, err := sql.Open("sqlite3", dbname)
	if CheckError(err) {
		os.Exit(0)
	}
	defer db.Close()
	VerifyCreate, err := ioutil.ReadFile("Create_table_verify.sql")
	UsersCreate, err := ioutil.ReadFile("Create_table_users.sql")
	SCOCreate, err := ioutil.ReadFile("Create_table_sco.sql")
	_,err=db.Exec(string(VerifyCreate))
	if CheckError(err) {
		os.Exit(0)
	}
	_,err=db.Exec(string(UsersCreate))
	if CheckError(err) {
		os.Exit(0)
	}
	_,err=db.Exec(string(SCOCreate))
	if CheckError(err) {
		os.Exit(0)
	}
}

func insert(file_name string, file_hash string) sql.Result {
	db, err := sql.Open("sqlite3", DB.dbName)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	result, err := db.Exec("insert into verify(file_name,file_hash) values ($1,$2)",
		file_name, file_hash)
	if err != nil {
		panic(err)
	}
	return result

}

func (d *dataBase) extractAllVerify() []Verify {
	db, err := sql.Open("sqlite3", DB.dbName)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	rows, err := db.Query("SELECT * FROM verify")
	defer rows.Close()

	data := []Verify{}

	for rows.Next() {
		d := Verify{}
		err := rows.Scan(&d.id, &d.File_name, &d.File_hash, &d.File_path)
		if err != nil {
			println(err)
			continue
		}
		//fmt.Print(d.file_name)
		data = append(data, d)
	}
	return data
}

func InsertVerify(file_name string, file_hash string, file_path string) sql.Result {
	db, err := sql.Open("sqlite3", DB.dbName)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	result, err := db.Exec("insert into verify(file_name,file_hash,file_path) values ($1,$2,$3)",
		file_name, file_hash, file_path)
	if err != nil {
		panic(err)
	}
	return result

}

func UserVerify(key string) User {
	db, err := sql.Open("sqlite3", DB.dbName)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	rows, err := db.Query("SELECT * FROM users where key = $1", key)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	d := User{}

	for rows.Next() {
		err := rows.Scan(&d.Id, &d.Username, &d.Password, &d.Key, &d.CryptoWord)
		if err != nil {
			println(err)
			continue
		}
		//fmt.Print(d.file_name)
	}
	return d
}
