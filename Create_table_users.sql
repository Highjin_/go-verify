CREATE TABLE users (
    id         INTEGER       PRIMARY KEY AUTOINCREMENT
                             UNIQUE
                             NOT NULL,
    username   VARCHAR (191) UNIQUE
                             NOT NULL,
    password   VARCHAR (255) NOT NULL,
    [key]      VARCHAR (256) UNIQUE,
    CryptoWord VARCHAR (10) 
);