package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
)

type myConfig struct {
	Title      string
	H1         string
	DBName     string
	ServerPort string
}
type DataView struct {
	/*
		Структура для отображения внутри странцы-шаблона
	*/
	Title      string
	H1         string
	VerifyData []Verify
	Auth       bool
	Usr        User
}

/*
60E8ED8916136566B4138DF73A54FC0FED9561DC15275AC6B0948EE75A5521EE
*/

func CheckError(err error)bool {
	/*
		Обработка ошибок
	*/
	result:=false
	if err != nil {
		fmt.Println(err)
		result=true
	}
	return result
}
func LoadConfig() myConfig {
	jsonConfig, err := ioutil.ReadFile("config.json")
	if(CheckError(err)){
		os.Exit(0)
	}
	var f myConfig
	err = json.Unmarshal(jsonConfig, &f)
	if(CheckError(err)){
		os.Exit(0)
	}
	fmt.Printf("Loaded config:%+v\n", f)
	return f
}

func main() {
	var isLoad = make(chan bool)
	Conf:=LoadConfig()
	go AutoLoad(Conf,isLoad)
	c:=<-isLoad
	for !c{
		c=<-isLoad
		fmt.Println("Loading Data...")
	}
	fmt.Println("Data is loaded.")
	data := DataView{Conf.Title, Conf.H1, GetVerify(), false, User{}}
	var KEY string


	fmt.Println("Starting server...")

	http.HandleFunc("/make", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprintf(writer, InsertFileHash(request.FormValue("file_name")))
	})
	http.HandleFunc("/login", func(writer http.ResponseWriter, request *http.Request) {
		KEY = request.FormValue("key")
		u := UserVerify(KEY)
		data.Usr = u
		if (data.Usr != User{}) {
			data.Auth = true
			var userCookie http.Cookie
			userCookie.Name = "jin"
			userCookie.Value = KEY
			http.SetCookie(writer, &userCookie)
		}
		http.Redirect(writer, request, "/", http.StatusMovedPermanently)

	})
	http.HandleFunc("/get", func(writer http.ResponseWriter, request *http.Request) {
		var data []Verify
		data = GetVerify()
		for _, d := range data {
			fmt.Fprintf(writer, d.getString()+"\n")

			fmt.Printf("%s\n", d.getString())
		}
	})
	http.HandleFunc("/add", func(writer http.ResponseWriter, request *http.Request) {
		file, handler, err := request.FormFile("file")
		CheckError(err)
		// read file bytes
		fileBytes, err := ioutil.ReadAll(file)
		CheckError(err)
		// write bytes to a localfile
		err = ioutil.WriteFile("upload/"+handler.Filename, fileBytes, 0644)
		v := Verify{0, handler.Filename, FileMD5("upload/" + handler.Filename), "upload/" + handler.Filename}
		v.Save()
		CheckError(err)
		http.Redirect(writer, request, "/", http.StatusAccepted)

	})
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		/*
			Функция входа на главную страницу приложения.
			Какждый раз при входе обновляются данные []Verify для отображения файлов в системе СКО.
		*/
		data.VerifyData = GetVerify()
		tmpl, err := template.ParseFiles("resources/index.html")
		if err != nil {
			fmt.Println(err)
			panic("При генерации шаблона произошла ошибка! Обратитесь к администратору")
		}
		tmpl.Execute(writer, data)
	})
	http.ListenAndServe(Conf.ServerPort, nil)
}
