CREATE TABLE sco (
    idVerify   INT      REFERENCES verify (id),
    owner      INT      REFERENCES users (id) 
                        NOT NULL,
    lastUpdate DATETIME
);
