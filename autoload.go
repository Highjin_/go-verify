package main

import (
	"fmt"
	"os"
)

func AutoLoad(Conf myConfig, LoadStatus chan bool) {
	fmt.Println("Autoload is start...")
	LoadStatus <- false
	if _, err := os.Stat(Conf.DBName); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			os.Create(Conf.DBName)
			CreateTables(Conf.DBName)

		} else {
			// other error
			fmt.Println(err)
			os.Exit(0)
		}
	}
	LoadStatus <- true

}
