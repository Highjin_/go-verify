package main

import (
	"database/sql"
)

type SCO struct {
	idVerify int
	owner int
	lastUpdate string
}

func SCOextractAll() []SCO{
	db,err :=sql.Open("sqlite3","node.db")
	if err != nil{
		panic(err)
	}
	defer db.Close()
	rows,err := db.Query("SELECT * FROM sco")
	defer rows.Close()

	data :=[]SCO{}

	for rows.Next(){
		d := SCO{}
		err := rows.Scan(&d.idVerify,&d.owner,&d.lastUpdate)
		if err != nil {
			println(err)
			continue
		}
		data = append(data,d)
	}
	return data
}
