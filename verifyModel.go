package main

type Verify struct {
	id int
	File_name string
	File_hash string
	File_path string
}

func (v *Verify) getString() string {
	var result string
	result= v.File_name+" : "+v.File_hash
	return result
}

func (v *Verify) Save()  {
	InsertVerify(v.File_name,v.File_hash,v.File_path)
}