CREATE TABLE verify (
    id        INTEGER       PRIMARY KEY AUTOINCREMENT,
    file_name VARCHAR (255) NOT NULL
                            UNIQUE,
    file_hash VARCHAR (255) UNIQUE
                            NOT NULL,
    file_path VARCHAR (255) UNIQUE
);